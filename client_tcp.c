/**
 * Basic TCP client
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <getopt.h>

typedef struct _Config
{
    short port;
    struct sockaddr_in addr_d;
    struct sockaddr_in addr_s;
    int num_frames;
    int frame_size;
} Config;

static Config _config;

Config* init_config()
{
    _config.num_frames = 8;
    _config.frame_size = 256;
    _config.port = 8080;
    _config.addr_d.sin_family = AF_INET;
    _config.addr_d.sin_port = htons(_config.port);
    _config.addr_d.sin_addr.s_addr = htonl(INADDR_ANY);
    return &_config;
}

int socket_open()
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
        error (1, errno, "socket error");
    return fd;
}

void do_work(int fd)
{
    static char* buffer[1024*4];
    memset (buffer, '\0', sizeof(buffer));
    
    memset (buffer, 'A', _config.frame_size);
    
    int ret = 0;
    for (int i=0; i<_config.num_frames; ++i)
    {
        ret = send (fd, buffer, _config.frame_size, 0);
        if (ret <= 0)
            fprintf(stderr, "[%d] send error\n", i);
        else
            printf("[%d] sent %d bytes\n", i, ret);
    }
}

void parse_args(int argc, char** argv)
{
    int c;
    
    init_config();
    
    while ((c=getopt(argc, argv, "l:n:pd:")) != -1)
    {
        // printf ("%c = %s\n", c, optarg);
        switch (c)
        {
            case 'n':
                _config.num_frames = atoi(optarg);
                break;
            case 'l':
                _config.frame_size = atoi(optarg);
                break;
            case 'p':
                _config.addr_d.sin_port = htons(atoi(optarg));
                break;
            case 'd':
                if (inet_pton(AF_INET, optarg, &_config.addr_d.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            default:
                fprintf(stderr, "unknown option");
        }
    }
    
    if (!_config.addr_d.sin_addr.s_addr)
        error(1, 0, "must specify (-d x.x.x.x) dst address");
    
    printf("size: %d\n", _config.num_frames);
    printf("len: %d\n", _config.frame_size);
    printf("address: %s\n", inet_ntoa(_config.addr_d.sin_addr));
    printf("port: %d\n", _config.port);
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    
    int fd = socket_open();
    
    if (connect(fd, (struct sockaddr*)&_config.addr_d, sizeof(_config.addr_d)) < 0) 
        error(1, errno, "connect");
    
    do_work (fd);
    
    if (close(fd) < 0)
        error(1, errno, "close");
    
    return 0;
}
