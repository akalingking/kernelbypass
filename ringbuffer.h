#pragma once
#include <arpa/inet.h>
#include <assert.h>
#include <error.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/filter.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <poll.h>
#include <pthread.h>
#include <sched.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "config.h"

extern Config *_config;

int setup_tx(char **ring)
{
    struct sockaddr_ll laddr = {};
    struct tpacket_req req = {};
    int fdt;

    fdt = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (fdt == -1)
        error(1, errno, "socket tx");

    laddr.sll_family = AF_PACKET;
    laddr.sll_protocol = htons(0);
    laddr.sll_ifindex = if_nametoindex("lo");
    if (!laddr.sll_ifindex)
        error(1, errno, "if_nametoindex");

    if (bind(fdt, (void *)&laddr, sizeof(laddr)))
        error(1, errno, "bind fdt");

    req.tp_block_size = getpagesize();
    req.tp_block_nr   = 1;
    req.tp_frame_size = getpagesize();
    req.tp_frame_nr   = 1;

    if (setsockopt(fdt, SOL_PACKET, PACKET_TX_RING, (void *)&req, sizeof(req)))
        error(1, errno, "setsockopt ring");

    *ring = mmap(0, req.tp_block_size * req.tp_block_nr, PROT_READ | PROT_WRITE, MAP_SHARED, fdt, 0);
    if (*ring == MAP_FAILED)
        error(1, errno, "mmap");

    return fdt;
}

int setup_rx()
{
    int fdr = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
    if (fdr == -1)
        error(1, errno, "socket rx");
    return fdr;
}

static void _build_packet(void *buffer, size_t buffer_len, char* payload, size_t payload_len)
{
    struct udphdr *udph;
    struct ethhdr *eth;
    struct iphdr *iph;
    size_t off = 0;

    memset(buffer, 0, buffer_len);
    assert (buffer_len > payload_len);

    eth = buffer;
    eth->h_proto = htons(ETH_P_IP);

    off += sizeof(*eth);
    iph = buffer + off;
    iph->ttl = 8;
    iph->ihl = 5;
    iph->version = 4;
    iph->saddr = htonl(_config->addr_s.sin_addr.s_addr);
    iph->daddr = htonl(_config->addr_d.sin_addr.s_addr);
    iph->protocol = IPPROTO_UDP;
    iph->tot_len = htons(buffer_len - off);
    iph->check = 0;

    off += sizeof(*iph);
    udph = buffer + off;
    udph->source = htons(_config->src_port);
    udph->dest	= htons(_config->dst_port);
    udph->len = htons(buffer_len - off);
    udph->check	= 0;

    off += sizeof(*udph);
    memcpy(buffer + off, payload, payload_len);
}


size_t send_packet(int fd, char* slot, char* payload, size_t payload_len)
{
    struct tpacket_hdr *header = (struct tpacket_hdr*)slot;
    int ret;

    while (header->tp_status != TP_STATUS_AVAILABLE)
        usleep(1000);

    _build_packet(slot + _config->eth_off, _config->frame_size, payload, payload_len);

    header->tp_len = _config->frame_size;
    header->tp_status = TP_STATUS_SEND_REQUEST;

    ret = sendto(fd, NULL, 0, 0, NULL, 0);
    if (ret == -1)
        error(1, errno, "sendto");
    
    return ret;
}

