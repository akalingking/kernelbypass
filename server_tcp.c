/**
 * Basic TCP Server
 */
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <error.h>
#include <errno.h>
#include <getopt.h>
#include <pthread.h>


typedef struct _Config
{
    struct sockaddr_in addr;
    bool resend;
} Config;

static Config _config;

Config* init_config()
{
    _config.resend = false;
    _config.addr.sin_family = AF_INET;
    _config.addr.sin_port = htons(8080);
    _config.addr.sin_addr.s_addr = htonl(INADDR_ANY);
    return &_config;
}

int socket_open()
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd <= 0)
        error(1, errno, "socket");
    
    int opt = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
        error(1, errno, "setsockopt");
    
    return fd;
}

void socket_bind(int fd)
{
    struct sockaddr_in* addr = &_config.addr;
    int addrlen = sizeof(*addr);
    
    if (bind(fd, (struct sockaddr*)addr, addrlen) < 0)
        error(1, errno, "bind");

    if (listen(fd, 3) < 0)
        error(1, errno, "listen");
    
}

void do_work(int fd)
{
    struct sockaddr_in addr;
    int addrlen = sizeof(addr);
    char buffer[1024*4];
    while (true)
    {
        printf("waiting for connection..\n");

        int clientfd = 0;
        if ((clientfd = accept(fd, (struct sockaddr*)&addr, (socklen_t*)&addrlen)) < 0)
        {
            perror("accept");
            continue;
        }
        else
        {
            printf("got connection from %s\n", inet_ntoa(addr.sin_addr));
            const char* client_addr = inet_ntoa(addr.sin_addr);
            while (true)
            {
                memset(buffer, '\0', sizeof(buffer));
                int rcv_size = read(clientfd, buffer, sizeof(buffer));
                if (rcv_size <= 0)
                    break;
                printf("rcv %d bytes from %s: '%s'\n", rcv_size, client_addr, buffer);
                if (_config.resend)
                {
                    int snd_size = send(clientfd, buffer, strlen(buffer), 0);
                    printf("%d bytes sent\n", snd_size);
                }
            }
        }
    }
}


void parse_args(int argc, char** argv)
{
    int c;
    init_config();
    while ((c=getopt(argc, argv, "p:s:r")) != -1)
    {
        switch (c)
        {
            case 'r':
                _config.resend = true;
                break;
            case 'p':
                {
                    int port = atoi(optarg);
                    _config.addr.sin_port = htons(port);
                }
                break;
            case 's':
                if (inet_pton(AF_INET, optarg, &_config.addr.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            default:
                fprintf(stderr, "unknown option");
        }
    }
    
    if (!_config.addr.sin_addr.s_addr)
        error(1, 0, "must specify server address");
    
    printf("address: %s\n", inet_ntoa(_config.addr.sin_addr));
    printf("port: %d\n", ntohs(_config.addr.sin_port));
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);
    
    int fd = socket_open();
    
    socket_bind (fd);
    
    do_work(fd);
    
    if (close(fd) < 0)
        error(1, errno, "close");

    return 0;
}
