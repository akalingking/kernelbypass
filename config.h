#pragma once

typedef struct _Config
{
    int client_mode;
    struct sockaddr_in addr_s;
    short src_port;
    struct sockaddr_in addr_d;
    short dst_port;
    int num_frames;
    int payload_size;
    int frame_size;
    int eth_off;

} Config;
