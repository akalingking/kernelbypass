#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import matplotlib.pyplot as plt
import pandas as pd

fticks_wo_bypass = "data/result_ticks_wo_bypass.csv"
ftimes_wo_bypass = "data/result_times_wo_bypass.csv"
fticks_w_bypass = "data/result_ticks_w_bypass.csv"
ftimes_w_bypass = "data/result_times_w_bypass.csv"

def main():
    n_frames = payload_size = None
    if len(sys.argv) >= 2:
        n_frames = sys.argv[1]
        payload_size = sys.argv[2]

    fig, axes = plt.subplots(figsize=(10,7), nrows=2, ncols=2, sharex=True)

    # cpu ticks
    ticks_wo_bypass = pd.read_csv(fticks_wo_bypass, header=None)
    ticks_wo_bypass = ticks_wo_bypass.reset_index(drop=True)
    ticks_wo_bypass.columns = ["iteration", "snd", "rcv"]

    ticks_w_bypass = pd.read_csv(fticks_w_bypass, header=None)
    ticks_w_bypass = ticks_w_bypass.reset_index(drop=True)
    ticks_w_bypass.columns = ["iteration", "snd", "rcv"]

    ticks_snd = pd.DataFrame()
    ticks_snd["wo-bypass"] = ticks_wo_bypass["snd"] / 1000.0
    ticks_snd["w-bypass"] = ticks_w_bypass["snd"] / 1000.0
    print(ticks_snd)

    axes[0,0].set_xlabel("iteration (snd)")
    axes[0,0].set_ylabel("cpu-ticks (x1000)")
    #axes[0,0].set_title("Send")
    ticks_snd.plot(ax=axes[0,0])

    ticks_rcv = pd.DataFrame()
    ticks_rcv["wo-bypass"] = ticks_wo_bypass["rcv"] / 1000.0
    ticks_rcv["w-bypass"] = ticks_w_bypass["rcv"] / 1000.0
    print (ticks_rcv)
    axes[0,1].set_xlabel("iteration (rcv)")
    #axes[0,1].set_ylabel("cpu-ticks (x1000)")
    #axes[0,1].set_title("Receive")
    ticks_rcv.plot(ax=axes[0,1])

    # timestamp
    times_wo_bypass = pd.read_csv(ftimes_wo_bypass, header=None)
    times_wo_bypass = times_wo_bypass.reset_index(drop=True)
    times_wo_bypass.columns = ["iteration", "snd", "rcv"]

    times_w_bypass = pd.read_csv(ftimes_w_bypass, header=None)
    times_w_bypass = times_w_bypass.reset_index(drop=True)
    times_w_bypass.columns = ["iteration", "snd", "rcv"]

    times_snd = pd.DataFrame()
    times_snd["wo-bypass"] = times_wo_bypass["snd"] / 1000.0
    times_snd["w-bypass"] = times_w_bypass["snd"] / 1000.0
    print(times_snd)

    axes[1,0].set_xlabel("iteration (send)")
    axes[1,0].set_ylabel("micro-seconds")
    #axes[1,0].set_title("Send")
    times_snd.plot(ax=axes[1,0])

    times_rcv = pd.DataFrame()
    times_rcv["wo-bypass"] = times_wo_bypass["rcv"] / 1000.0
    times_rcv["w-bypass"] = times_w_bypass["rcv"] / 1000.0
    print(times_rcv)
    axes[1,1].set_xlabel("iteration (receive)")
    #axes[1,1].set_ylabel("micro-seconds")
    #axes[1,1].set_title("Receive")
    times_rcv.plot(ax=axes[1,1])

    if n_frames is not None and payload_size is not None:
        fig.suptitle("{} Frames of {} Bytes".format(n_frames, payload_size))
    plt.tight_layout()
    plt.show()
    fig.savefig("./data/plot.pdf", bbox_inches='tight')


if __name__ == "__main__":
    main()
