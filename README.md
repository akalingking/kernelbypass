# Fast Kernel Bypassed Socket Operations

## Description
``` ascii

Fast packet I/O is a vital component of network service 
applications for media streaming (audio and video) 
and large data acquisition and transactions.

High speed socket is normally implemented using hardware 
accelerators such as:

[1] SolarFlare
[2] Infiniband

to attain low to ultra low latency packet transfers directly
between applications' user space memory and IO device buffers.
```

## Benchmarks
``` ascii
Simplified back to back setup on UDP with ring buffers:

[1] client-->packet-->loopback-->packet-->server -->
                                                   |
[2] client<--packet<--loopback<--packet<--server <--                                           
```

### 100 x 256 byte frames 
![100 x 256 byte frame](./data/100_x_256.png)
### 100 x 512 byte frames 
![100 x 512 byte frame](./data/100_x_512.png)
### 100 x 1024 byte frames 
![100 x 1024 byte frame](./data/100_x_1024.png)

## Build
[1] Without bypass 
    gcc ./udpbasic.c -o udpbasic

[2] With bypass
    gcc ./udpkernelbypass.c -o kernelbypass

[3] Run binary with '-c' 0 for server mode and '-c 1' for client mode

[4] run 'python3 plotresult.py' to feed on generated ./data to see plots


## References
[1] https://orbi.uliege.be/bitstream/2268/181954/1/userspaceio.pdf

[2] https://www.gnu.org/software/libc/manual/html_node/Memory_002dmapped-I_002fO.html

[3] https://blog.cloudflare.com/kernel-bypass/

[4] https://www.infinibandta.org/about-infiniband

[5] https://www.openonload.org
