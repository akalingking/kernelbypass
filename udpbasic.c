#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include "config.h"
#include "metrics.h"

Config* _config = NULL;

Config* init_config(Config* in)
{
    assert (in != NULL);
    in->client_mode = 1;
    in->num_frames = 5;
    in->payload_size = 256;
    in->src_port = 8000;
    in->dst_port = 8001;

    in->addr_s.sin_family = AF_INET;
    in->addr_s.sin_port = htons(in->src_port);
    if (inet_pton(AF_INET, "127.0.0.1", &in->addr_s.sin_addr) <= 0)
        error(1, errno, "inet_pton");

    in->addr_d.sin_family = AF_INET;
    in->addr_d.sin_port = htons(in->dst_port);
    if (inet_pton(AF_INET, "127.0.0.1", &in->addr_d.sin_addr) <= 0)
        error(1, errno, "inet_pton");
    return in;
}

void parse_args(int argc, char** argv)
{
    int c;

    while ((c=getopt(argc, argv, "c:l:n:p:d:s:")) != -1)
    {
        switch (c)
        {
            case 'c':
                _config->client_mode = atoi(optarg);
                if (_config->client_mode == 0)
                {
                    // Check if ports were unchanged from default, if so swap
                    // otherwise need to explicitly specify in command line..
                    if (_config->src_port == 8000 && _config->dst_port == 8001)
                    {
                        _config->src_port = 8001;
                        _config->dst_port = 8000;
                        _config->addr_s.sin_port = htons(_config->src_port);
                        _config->addr_d.sin_port = htons(_config->dst_port);
                    }
                }
                break;
            case 'a':
                _config->src_port = atoi(optarg);
                _config->addr_s.sin_port = htons(_config->src_port);
                break;
            case 'b':
                _config->dst_port = atoi(optarg);
                _config->addr_d.sin_port = htons(_config->dst_port);
                break;
            case 'n':
                _config->num_frames = atoi(optarg);
                break;
            case 'l':
                _config->payload_size = atoi(optarg);
                break;
            case 'd':
                assert (optarg != NULL);
                if (inet_pton(AF_INET, optarg, &_config->addr_d.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            case 's':
                assert (optarg != NULL);
                if (inet_pton(AF_INET, optarg, &_config->addr_s.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            default:
                fprintf(stderr, "unknown option");
        }
    }

    if (!_config->addr_d.sin_addr.s_addr || !_config->addr_s.sin_addr.s_addr)
        error(1, 0, "Must specify dst address");

    printf ("Size: %d\n", _config->num_frames);
    printf ("Len: %d\n", _config->payload_size);
    printf ("Src port: %d\n", _config->src_port);
    printf ("Dst port: %d\n", _config->dst_port);
    printf ("Src ip: %s\n", inet_ntoa(_config->addr_s.sin_addr));
    printf ("Dst ip: %s\n", inet_ntoa(_config->addr_d.sin_addr));
    printf ("Running in '%s' mode\n", (_config->client_mode)?"client":"server");
}

int socket_open()
{
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
        error (1, errno, "socket error");
    return fd;
}

void socket_bind(int fd)
{
    struct sockaddr_in* addr = &_config->addr_s;
    int addrlen = sizeof(*addr);

    if (bind(fd, (struct sockaddr*)addr, addrlen) < 0)
        error(1, errno, "bind");
}

void do_client_work()
{
    // initialize inbound/outbound socket
    int fd = socket_open();

    // initialize buffers
    size_t payload_size = _config->payload_size;
    char txbuffer[payload_size+1];
    char rxbuffer[payload_size+1];
    memset (txbuffer, '\0', payload_size+1);
    memset (txbuffer, 'A', payload_size);

    // metric containers
    uint64_t *ticks_tx = malloc(sizeof(uint64_t) * _config->num_frames);
    uint64_t *ticks_rx = malloc(sizeof(uint64_t) * _config->num_frames);
    unsigned long *times_tx = malloc(sizeof(unsigned long) * _config->num_frames);
    unsigned long *times_rx = malloc(sizeof(unsigned long) * _config->num_frames);

    int ret = 0;
    struct sockaddr *dst_addr = ( struct sockaddr*)&_config->addr_d;
    socklen_t dst_addr_len = sizeof(*dst_addr);
    char *dst_addr_name = inet_ntoa(_config->addr_d.sin_addr);
    uint64_t tick_txstart, tick_txend, tick_rxend, time_txstart, time_txend, time_rxend;
    for (int i=0; i<_config->num_frames; ++i)
    {
        tick_txstart = rdtscp(); time_txstart = gettimeofday_ns();

        ret = sendto (fd, txbuffer, _config->payload_size, 0, (struct sockaddr*)dst_addr, sizeof(*dst_addr));
        printf("[%d] sent %d bytes\n", i, ret);

        tick_txend = rdtsc(); time_txend = gettimeofday_ns();

        if (ret > 0)
        {
            size_t total_rcv_size = 0;
            memset (rxbuffer, '\0', payload_size);
            while (total_rcv_size < payload_size)
            {
                //int rcv_size = read(fd, rxbuffer+total_rcv_size, payload_size-total_rcv_size);
                int rcv_size = recvfrom(fd, rxbuffer+total_rcv_size, payload_size-total_rcv_size, 0, dst_addr, &dst_addr_len);
                if (rcv_size <= 0)
                    break;

                //printf("[%d] rcvd %d bytes from %s: '%s'\n", i, rcv_size, dst_addr_name, rxbuffer);
                printf("[%d] rcvd %d bytes from %s\n", i, rcv_size, dst_addr_name);
                total_rcv_size += rcv_size;
            }

            tick_rxend = rdtsc(); time_rxend = gettimeofday_ns();

            ticks_tx[i] = tick_txend - tick_txstart;
            ticks_rx[i] = tick_rxend - tick_txend;
            times_tx[i] = time_txend - time_txstart;
            times_rx[i] = time_rxend - time_txend;
        }
        else
            fprintf(stderr, "send error\n");
    }


    // Record metrics data
    FILE *fhandle = fopen("./data/result_ticks_wo_bypass.csv", "w");
    printf ("\nCpu ticks : (snd, rcv)\n");
    for (size_t i = 0; i < _config->num_frames; i++)
    {
        printf("(%" PRIu64 ",\t%" PRIu64 ")\n", ticks_tx[i], ticks_rx[i]);
        if (fhandle) fprintf (fhandle, "%ld, %" PRIu64 ", %" PRIu64 "\n", i, ticks_tx[i], ticks_rx[i]);
    }
    if (fhandle) fclose(fhandle);

    fhandle = fopen("./data/result_times_wo_bypass.csv", "w");
    printf ("\nNanosecs : (snd, rcv)\n");
    for (size_t i = 0; i < _config->num_frames; i++)
    {
        printf("(%lu,\t%lu)\n", times_tx[i], times_rx[i]);
        if (fhandle) fprintf (fhandle, "%ld, %lu, %lu\n", i, times_tx[i], times_rx[i]);
    }
    if (fhandle) fclose(fhandle);

    if (close(fd) < 0)
        error(1, errno, "close");

    free (ticks_tx);
    free (ticks_rx);
    free (times_tx);
    free (times_rx);
}

void do_server_work()
{
    struct sockaddr_in client_addr;
    socklen_t addrlen = sizeof(client_addr);
    size_t payload_size = _config->payload_size;
    char buffer[payload_size];

    int fd = socket_open();

    socket_bind (fd);

    while (true)
    {
        printf("Waiting for connection..\n");

        size_t rcv_size = recvfrom(fd, buffer, payload_size, MSG_WAITALL, (struct sockaddr*)&client_addr, &addrlen);
        printf ("Rcv %ld bytes from %s\n", rcv_size, inet_ntoa(client_addr.sin_addr));

        if (rcv_size)
        {
            size_t snd_size = sendto(fd, buffer, strlen(buffer), MSG_CONFIRM, (struct sockaddr*)&client_addr, addrlen);
            printf("%ld bytes sent\n", snd_size);
        }
    }


    if (close(fd) < 0)
        error(1, errno, "close");
}

int main(int argc, char** argv)
{
    printf ("Starting '%s'..\n", argv[0]);

    Config config;

    _config = init_config(&config);
    assert (_config != NULL);

    parse_args(argc, argv);

    if (_config->client_mode)
        do_client_work ();
    else
        do_server_work();

    printf ("Stopping '%s'..\n", argv[0]);

    return 0;
}


