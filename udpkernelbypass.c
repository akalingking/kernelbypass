
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include "metrics.h"
#include "ringbuffer.h"

static size_t header_size = sizeof (struct ethhdr) + sizeof (struct iphdr) + sizeof (struct udphdr);

Config* _config = NULL;

Config* init_config(Config* in)
{
    assert (in != NULL);
    in->client_mode = 1;
    in->num_frames = 5;
    in->payload_size = 512;
    in->frame_size = header_size + in->payload_size+1;
    in->src_port = 8000;
    in->dst_port = 8001;
    in->eth_off = TPACKET_HDRLEN - sizeof(struct sockaddr_ll);

    in->addr_d.sin_family = AF_INET;
    in->addr_d.sin_port = htons(in->dst_port);
    if (inet_pton(AF_INET, "127.0.0.1", &in->addr_d.sin_addr) <= 0)
        error(1, errno, "inet_pton");

    in->addr_s.sin_family = AF_INET;
    in->addr_s.sin_port = htons(in->src_port);
    if (inet_pton(AF_INET, "127.0.0.1", &in->addr_s.sin_addr) <= 0)
        error(1, errno, "inet_pton");

    return in;
}

void parse_args(int argc, char** argv)
{
    int c;

    while ((c=getopt(argc, argv, "c:l:n:p:d:s:")) != -1)
    {
        switch (c)
        {
            case 'c':
                _config->client_mode = atoi(optarg);
                if (_config->client_mode == 0)
                {
                    if (_config->src_port == 8000 && _config->dst_port == 8001)
                    {
                        _config->src_port = 8001;
                        _config->dst_port = 8000;
                        _config->addr_s.sin_port = htons(_config->src_port);
                        _config->addr_d.sin_port = htons(_config->dst_port);
                     }
                }
                break;
            case 'a':
                _config->src_port = atoi(optarg);
                _config->addr_s.sin_port = htons(_config->src_port);
                break;
            case 'b':
                _config->dst_port = atoi(optarg);
                 _config->addr_d.sin_port = htons(_config->dst_port);
                break;
            case 'n':
                _config->num_frames = atoi(optarg);
                break;
            case 'l':
                _config->payload_size = atoi(optarg);
                _config->frame_size = header_size + _config->payload_size + 1;
                break;
            case 'd':
                assert (optarg != NULL);
                if (inet_pton(AF_INET, optarg, &_config->addr_d.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            case 's':
                assert (optarg != NULL);
                if (inet_pton(AF_INET, optarg, &_config->addr_s.sin_addr) <= 0)
                    error(1, errno, "inet_pton");
                break;
            default:
                fprintf(stderr, "'%c' unknown option\n", c);
        }
    }

    if (!_config->addr_d.sin_addr.s_addr || !_config->addr_s.sin_addr.s_addr)
        error(1, 0, "Must specify dst address");

    printf ("Payload size: %d\n", _config->payload_size);
    printf ("Frame size: %d\n", _config->frame_size);
    printf ("Num frames: %d\n", _config->num_frames);
    printf ("Src ip: %s\n", inet_ntoa(_config->addr_s.sin_addr));
    printf ("Src port: %d\n", _config->src_port);
    printf ("Dst ip: %s\n", inet_ntoa(_config->addr_d.sin_addr));
    printf ("Dst port: %d\n", _config->dst_port);
    printf ("Running as '%s' mode\n", _config->client_mode?"client":"server");
}

void do_server_work()
{
    printf("do_server_work entry\n");

    size_t frame_size = _config->frame_size;
    short src_port = _config->src_port;
    short dst_port = _config->dst_port;
    short client_src_port, client_dst_port;

    char buffer_rx[_config->frame_size];
    char *buffer_tx;
    int ret;

    int fd_tx = setup_tx(&buffer_tx);
    assert (fd_tx > 0);

    int fd_rx =  setup_rx();
    assert (fd_rx > 0);

    while (true)
    {
        memset(buffer_rx, '\0', frame_size);
        printf("Reading buffer..\n");
        ret = read(fd_rx, buffer_rx, sizeof(buffer_rx));
        printf ("Read %d bytes\n", ret);
        if (ret <= 0)
        {
            error(1, errno, "read");
        }
        else
        {
            size_t total_bytes = ret;
            printf("do_work rcvd %d of %ld bytes\n", ret, frame_size);
            while (total_bytes < _config->frame_size)
            {
                ret = read(fd_rx, buffer_rx+total_bytes, sizeof(buffer_rx)- total_bytes);
                if (ret > 0)
                {
                    total_bytes += ret;
                }
            
            }

            // decode inbound frame
            struct ethhdr *eth;
            struct iphdr *iph;
            struct udphdr *udph;
            size_t offset;

            eth = (struct ethhdr*)buffer_rx;
            offset = sizeof(*eth);
            iph = (struct iphdr*) (buffer_rx + offset);
            struct in_addr saddr;
            saddr.s_addr = ntohl(iph->saddr);
            printf ("Client address '%s'\n", inet_ntoa(saddr));
            offset += sizeof(*iph);

            udph = (struct udphdr*) (buffer_rx + offset);
            if (udph != NULL)
            {
                if (saddr.s_addr == _config->addr_s.sin_addr.s_addr)
                {
                    client_src_port = ntohs(udph->source);
                    client_dst_port = ntohs(udph->dest);
                    size_t len = ntohs(udph->len);
                    printf ("Client port source=%d dest=%d config=(src:%d dst:%d)len=%ld\n", client_src_port, client_dst_port,
                        src_port, dst_port, len);
                    if (client_src_port == dst_port && client_dst_port == src_port)
                    {
                        char* payload;
                        offset += sizeof(*udph);
                        payload = buffer_rx + offset;
                        //printf ("Payload: offset=%ld %ld : '%s'\n", offset, strlen(payload), payload);
                        printf ("Payload: offset=%ld %ld\n", offset, strlen(payload));

                        // Return payload
                        ret = send_packet(fd_tx, buffer_tx, payload, strlen(payload));
                        printf ("Sent %d bytes\n", ret);

                    }
                    else
                        printf ("Invalid source=%d(%d) dest=%d(%d) ports, ignoring packet..\n", client_src_port, dst_port, client_dst_port, src_port);
                }
                else
                {
                    printf ("Invalid destination address: %s\n", inet_ntoa(saddr));
                }
            }
            else
                printf ("Invalid protocol\n");
        }
    }

    if (close(fd_tx) < 0)
        error (1, errno, "close tx");
    if (close(fd_rx) < 0)
        error (1, errno, "close rx");

    printf("do_server_work exit\n");
}

void do_client_work()
{
    printf("do_client_work entry\n");

    size_t num_frames = _config->num_frames;
    size_t frame_size = _config->frame_size;
    size_t payload_size = _config->payload_size;

    char rxbuffer[frame_size];
    int fdrx = setup_rx();

    char txbuffer[payload_size];

    memset (txbuffer, '\0', payload_size);
    memset (txbuffer, 'A', payload_size);

    char *ring_buffer_tx;
    int fdtx =  setup_tx(&ring_buffer_tx);

    // setup metric containers
    uint64_t *times_send = malloc(sizeof(uint64_t) * num_frames);
    uint64_t *times_recv = malloc(sizeof(uint64_t) * num_frames);

    unsigned long *ms_send = malloc(sizeof(unsigned long) * num_frames);
    unsigned long *ms_recv = malloc(sizeof(unsigned long) * num_frames);

    int ret = 0;
    for (int i=0; i<num_frames; ++i)
    {
        uint64_t tstart = rdtscp();
        uint64_t ms_txstart = gettimeofday_ns();

        ret = send_packet(fdtx, ring_buffer_tx, txbuffer, payload_size);
        printf("do_work: sent %d bytes\n", ret);

        uint64_t tsend = rdtsc();
        uint64_t ms_txend = gettimeofday_ns();

        if (ret > 0)
        {
            printf("[%d] sent %d bytes\n", i, ret);

            // wait for echo
            size_t rcv_total_size = 0;
            while (rcv_total_size < frame_size)
            {
                memset (rxbuffer, '\0', frame_size);
                int rcv_size = read(fdrx, rxbuffer+rcv_total_size, frame_size-rcv_total_size);
                if (rcv_size <= 0)
                    break;
//                const char* remote_addr = inet_ntoa(_config->addr_d.sin_addr);
                //printf("[%d] rcvd %d bytes\n", i, rcv_size);
                rcv_total_size += rcv_size;
            }
//            printf("[%d] rcvd %ld bytes '%s'\n", i, rcv_total_size, rxbuffer);
            printf("[%d] rcvd %ld bytes\n", i, rcv_total_size);

            uint64_t tend = rdtsc();
            times_send[i] = tsend - tstart;
            times_recv[i] = tend - tsend;

            uint64_t ms_rxend = gettimeofday_ns();
            ms_send[i] = ms_txend - ms_txstart;
            //printf("usecond %lu %lu\n", ms_txstart, ms_txend);
            ms_recv[i] = ms_rxend - ms_txend;
        }
        else
            fprintf(stderr, "Send error\n");
    }

    FILE *fhandle = fopen("./data/result_ticks_w_bypass.csv", "w");
    printf ("Cpu ticks : (snd, rcv)\n");
    for (size_t i = 0; i < num_frames; i++)
    {
        printf("(%" PRIu64 ",\t%" PRIu64 ")\n", times_send[i], times_recv[i]);
        fprintf(fhandle, "%ld, %" PRIu64 ",%" PRIu64"\n", i, times_send[i], times_recv[i]);
    }
    fclose(fhandle);

    fhandle = fopen("./data/result_times_w_bypass.csv", "w");
    printf ("Nano-secs: (snd, rcv)\n");
    for (size_t i = 0; i < num_frames; i++)
    {
        printf("(%lu,\t%lu)\n", ms_send[i], ms_recv[i]);
        fprintf(fhandle, "%ld, %lu, %lu\n", i, ms_send[i], ms_recv[i]);
    }
    fclose(fhandle);

    free (times_send);
    free (times_recv);
    free (ms_send);
    free (ms_recv);

    if (close(fdtx) < 0)
        error(1, errno, "close");

    if (close(fdrx) < 0)
        error(1, errno, "close");

    printf("do_client_work exit\n");
}

int main(int argc, char** argv)
{
    printf ("Starting '%s'..\n", argv[0]);

    Config config;
    
    _config = init_config(&config);

    assert (_config != NULL);

    parse_args(argc, argv);

    if (_config->client_mode)
        do_client_work();
    else
        do_server_work();

    printf ("Stopping '%s'..\n", argv[0]);

    return 0;
}




