#pragma once
#include <inttypes.h>
#include <sys/time.h>
#include <time.h>

uint64_t rdtsc()
{
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

uint64_t rdtscp()
{
    unsigned int lo,hi;
    __asm__ __volatile__ ("rdtscp" : "=a" (lo), "=d" (hi));
    return ((uint64_t)hi << 32) | lo;
}

uint64_t gettimeofday_ns()
{
    struct timespec val;
    clock_gettime(CLOCK_MONOTONIC, &val);
    return (val.tv_sec* 1000000000) + val.tv_nsec;
}